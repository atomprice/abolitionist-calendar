---
title: "About"
date: 2020-01-25T15:01:37Z
draft: false
---

There are many groups across London who are working, in one way or another, towards a world without prisons. 

This site is a place where we can find and advertise our events, with the aim of increasing collaboration. 

{{< figure src="/images/trans_action_no_cages.jpg" title="No Cages In Our Feminism">}}
