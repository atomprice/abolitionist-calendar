---
title: "Freedom Fighters Feast February"
date: 2020-01-25T16:11:33Z
ExpiryDate: 2020-02-24
eventDate: 2020-02-23
tags: ["Freedom Fighters Feast", "Cradle Community"]
draft: false
---

Regular casual dinner for Black and brown people either already involved or looking to get involved in the struggle against state violence.  

<!--more-->

* Time: 7--10 pm
* Location: [The Advocacy Academy Campus](https://goo.gl/maps/zRcreo7aeJbzrGhV9), 5--7 Vining St, Brixton, London SW9 8QA

RSVP will be through eventbrite: please keep an eye on this page for a link. Dinner will be provided for free. People looking to support this event (especially financially comfortable white allies), please consider donating to [Cradle Community](https://www.gofundme.com/f/cradlecommunity/donate)
