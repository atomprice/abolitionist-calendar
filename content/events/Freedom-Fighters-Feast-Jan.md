---
title: "Freedom Fighters Feast January"
date: 2020-01-25T16:11:33Z
ExpiryDate: 2020-02-01
eventDate: 2020-01-31
tags: ["Freedom Fighters Feast", "Cradle Community"]
draft: false
---

Regular casual dinner for Black and brown people either already involved or looking to get involved in the struggle against state violence.  

<!--more-->

* Time: 7--10 pm
* Location: [The Advocacy Academy Campus](https://goo.gl/maps/zRcreo7aeJbzrGhV9), 5--7 Vining St, Brixton, London SW9 8QA

Please RSVP on [eventbrite](https://www.eventbrite.co.uk/e/freedom-fighters-feast-tickets-89600953865). Dinner will be provided for free. People looking to support this event (especially financially comfortable white allies), please consider donating to [Cradle Community](https://www.gofundme.com/f/cradlecommunity/donate)
