---
title: "IWOC Monthly"
date: 2020-01-25T16:03:45Z
ExpiryDate: 2020-02-11
eventDate: 2020-02-10
tags: ["IWOC"]
draft: false
---

London IWOC's monthly catch up. Newcomers welcome!

<!--more-->

* Time: 18:30 -- 20:30
* Location: [New Cut Housing Co-op](https://goo.gl/maps/DtkHyyPe28HesTc96), 106 The Cut, London SE 1 8LN

Wheelchair accessible. 5 minute walk from either Waterloo Station or Southwark Tube.

